<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Jadwal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Jadwal::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            //'id' => 'required|string',
            'nis' => 'required|unique:siswa,nis',
            'nama' => 'required|string',
            'gender' => 'required|IN:laki-laki,perempuan',
            'tempat_lahir' => 'required|string',
            'tgl_lahir' => 'required|string',
            'email' => 'required|string|unique:siswa,email',
            'nama_ortu' => 'nullable|string',
            'alamat' => 'nullable|string',
            'phone_number' => 'nullable|string',
            'kelas_id' => 'nullable|integer',
        ]);

        if($validator->fails()){
            $msg = $validator->errors();

            return $this->failedResponse($msg,422);
        }

        $siswa = new Siswa();
        $siswa->id = $request->id;
        $siswa->nis = $request->nis;
        $siswa->nama = $request->nama;
        $siswa->gender = $request->gender;
        $siswa->tempat_lahir = $request->tempat_lahir;
        $siswa->tgl_lahir = $request->tgl_lahir;
        $siswa->email = $request->email;
        $siswa->nama_ortu = $request->nama_ortu;
        $siswa->alamat = $request->alamat;
        $siswa->phone_number = $request->phone_number;
        $siswa->kelas_id = $request->kelas_id;
        $saveSiswa = $siswa->save();

        if ($saveSiswa){
            return $this->success($siswa,201);
        }else{
            return $this->failedResponse('Siswa gagal ditambahkan!',500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function show(Jadwal $jadwal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jadwal $jadwal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jadwal $jadwal)
    {
        //
    }
}
